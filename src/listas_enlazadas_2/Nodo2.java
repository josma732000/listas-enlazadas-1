/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas_enlazadas_2;

/**
 *
 * @author Jose
 */
class Nodo2 {
      public String nombre;    // Valor a almacenar
    public Nodo2 siguiente;  // Puntero, del mismo tipo de la clase
    
    // Constructor para insertar el dato
    public Nodo2(String d){
        this.nombre = d;
    }
    // Constructor para insertar al inicio de la lista
    public Nodo2(String d, Nodo2 n){
        nombre = d;
        siguiente = n;
    }
}
