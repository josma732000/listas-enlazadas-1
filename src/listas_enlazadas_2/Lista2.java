/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas_enlazadas_2;

/**
 *
 * @author Jose
 */
class Lista2 {
    protected Nodo2 inicio, fin;   
    public Lista2(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio2(String elemento){
        inicio = new Nodo2(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada2(){
        Nodo2 recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.nombre+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    
    public String borrarDelInicio2(){
        String elemento = inicio.nombre;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
}