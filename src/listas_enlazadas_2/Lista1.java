/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listas_enlazadas_2;

/**
 *
 * @author Jose
 */
class Lista1 {
      protected Nodo1 inicio, fin;   // Punteros para donde esta el inicio y el fin
    public Lista1(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio(String elemento){
        inicio = new Nodo1(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada(){
        Nodo1 recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.nombre+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    // Metodo para eliminar un nodo del inicio
    public String borrarDelInicio(){
        String elemento = inicio.nombre;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
}
